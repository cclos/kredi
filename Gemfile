# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.1.0'

gem 'bcrypt', '~> 3.1.7'
gem 'dotenv-rails'
gem 'jbuilder', '~> 2.7'
gem 'jwt'
gem 'listen'
gem 'net-imap', require: false
gem 'net-pop', require: false
gem 'net-smtp', require: false
gem 'pg'
gem 'pry'
gem 'puma', '~> 5.0'
gem 'rack-cors'
gem 'rails', '~> 6.1.6', '>= 6.1.6.1'
gem 'redis-rails', '~> 5.0.2'
gem 'rqrcode', '~> 2.0'
gem 'sidekiq', '~>6.5.0'

group :development do
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'rubocop-performance', require: false
  gem 'rubocop-rspec', require: false
  gem 'shoulda-matchers', '~> 4.0'
  gem 'simplecov'
  gem 'web-console', '>= 4.1.0'
end

group :test do
  gem 'ffaker'
  gem 'rspec-rails'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'factory_bot_rails'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
