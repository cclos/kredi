# frozen_string_literal: true

class CreateInvoices < ActiveRecord::Migration[6.1]
  def change
    create_table :invoices, id: false do |t|
      t.uuid :uuid, primary_key: true
      t.integer :amount
      t.string :status
      t.string :currency
      t.string :cfdi_digital_stamp
      t.datetime :emitted_at
      t.datetime :expires_at
      t.references :emitter, null: false, foreign_key: { to_table: 'users' }
      t.references :receiver, null: false, foreign_key: { to_table: 'users' }
      t.timestamps
    end
  end
end
