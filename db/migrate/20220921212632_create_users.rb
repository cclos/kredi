# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :username
      t.string :rfc
      t.string :password_digest, null: false, default: ''
      t.timestamps
    end

    add_index :users, :rfc, unique: true
  end
end
