# frozen_string_literal: true

require 'rails_helper'
require 'ffaker'

RSpec.describe Invoice, type: :model do
  subject { described_class.new }

  it 'is valid with valid attributes' do
    expect(subject).not_to be_valid
  end

  context 'require two users to create an invoice' do
    let(:emitter) { create(:user, id: 1) }
    let(:receiver) { create(:user, id: 2, username: 'username', rfc: 'naked') }

    it 'is valid with valid attributes' do
      subject.uuid = SecureRandom.uuid
      subject.amount = rand(1..10)
      subject.status = 'pending'
      subject.created_at = DateTime.now
      subject.updated_at = DateTime.now
      subject.emitted_at = DateTime.now
      subject.emitter_id = emitter.id
      subject.receiver_id = receiver.id

      expect(subject).to be_valid
    end
  end
end
