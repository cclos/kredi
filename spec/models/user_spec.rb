# frozen_string_literal: true

RSpec.describe User, type: :model do
  subject { described_class.new }

  it 'is valid with valid attributes' do
    expect(subject).not_to be_valid
  end

  it 'is valid with valid attributes' do
    subject.name = FFaker::Name.unique.name
    subject.rfc = FFaker::Name.unique.name
    subject.username = FFaker::Name.unique.name
    subject.created_at = DateTime.now
    subject.updated_at = DateTime.now
    subject.password = FFaker::Internet.password
    expect(subject).to be_valid
  end
end
