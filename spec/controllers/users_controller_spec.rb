# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let(:valid_attributes) do
    { name: 'Test name',
      username: 'Test',
      rfc: 'Test',
      password: '123456',
      password_confirmation: '123456' }
  end

  let(:user) { create(:user, username: 'admin', password: 'admin123') }

  describe 'POST #create' do
    it 'returns a success response' do
      post :create, params: valid_attributes
      expect(response).to be_successful
    end
  end

  describe 'GET #index' do
    before do
      request.params[:username] = 'admin'
      request.params[:password] = 'admin123'
      login(user)
    end

    it 'returns a success response' do
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe 'DELETE #destroy' do
    it 'returns a success response' do
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end
end
