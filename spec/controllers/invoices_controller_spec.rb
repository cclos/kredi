# frozen_string_literal: true

require 'rails_helper'

RSpec.describe InvoicesController, type: :controller do
  let(:valid_attributes) do
    { name: 'Test name',
      username: 'Test',
      rfc: 'Test',
      password: '123456',
      created_at: DateTime.now,
      updated_at: DateTime.now }
  end

  let(:valid_session) { {} }

  describe 'POST #create' do
    it 'returns a success response' do
      post :create, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #index' do
    it 'returns a success response' do
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe 'DELETE #destroy' do
    it 'returns a success response' do
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end
end
