# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { 'pedro torees' }
    username { 'pedro' }
    rfc { 'pedro123' }
    password { 'secret!' }
    created_at { DateTime.now }
    updated_at { DateTime.now }
  end
end
