# frozen_string_literal: true

require 'rqrcode'

# Invoices API
class InvoicesController < ApplicationController
  before_action :authorize_request
  skip_before_action :verify_authenticity_token

  def index
    @invoices = search
    render json: @invoices, status: :ok
  end

  def create
    @invoice = Invoice.new(invoice_params)

    if @invoice.save
      render json: :ok
    else
      render json: :bad_request
    end
  end

  def destroy
    @invoice = Invoice.find(params[:id])
    @invoice.destroy

    render json: :ok
  rescue ActiveRecord::RecordNotFound
    render json: :bad_request
  end

  def update
    @invoice = Invoice.find(uuid: params[:id])
    @invoice.update(invoice_params)

    render json: :ok
  rescue ActiveRecord::RecordNotFound
    render json: :bad_request
  end

  def upload
    content = upload_params[:files].map { |file| file.tempfile.read }
    InvoiceUploadJob.perform_later(content)

    render json: :ok
  end

  def search
    return Invoice.all if request.query_parameters.empty?

    @invoices = Invoice.where(nil)
    request.query_parameters.each do |key, value|
      @invoices = @invoices.public_send("filter_by_#{key}", value) if value.present?
    end

    @invoices
  end

  def qr
    qrcode = RQRCode::QRCode.new(params[:cfdi_digital_stamp])

    svg = qrcode.as_svg(
      color: '000',
      shape_rendering: 'crispEdges',
      module_size: 10,
      standalone: true,
      use_path: true
    )

    respond_to do |format|
      format.svg { render inline: svg }
    end
  end

  private

  def invoice_params
    params.require(:invoice).permit(:emitter_id, :status, :amount, :receiver_id, :currency)
  end

  def upload_params
    params.permit(files: [])
  end

  def search_params
    params.permit(:uuid, :status, :amount, :emitter, :receiver, :emission_date)
  end

  def qr_params
    params.require(:invoice).permit(:cfdi_digital_stamp)
  end
end
