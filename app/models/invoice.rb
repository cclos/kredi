# frozen_string_literal: true

class Invoice < ApplicationRecord
  alias_attribute :user, :emitter
  alias_attribute :user, :receiver

  belongs_to :emitter, class_name: 'User'
  belongs_to :receiver,  class_name: 'User'

  validates :emitter, :receiver, :status, :amount, :emitted_at, :cfdi_digital_stamp, presence: true

  scope :filter_by_status, ->(status) { where status: }
  scope :filter_by_amount, ->(from, to) { where('amount between ? and ?', from, to) }
  scope :filter_by_emitter, ->(id) { where('emitter_id = ?', id) }
  scope :filter_by_receiver, ->(id) { where('receiver_id = ?', id) }
  scope :filter_by_emission_date, ->(date) { where('date(emitted_at) =?', date.to_s) }

  def initialize(params)
    super

    self.uuid = SecureRandom.uuid
    self.cfdi_digital_stamp = SecureRandom.uuid
    self.emitted_at = DateTime.now
    self.expires_at = DateTime.now
    self.created_at = DateTime.now
    self.updated_at = DateTime.now
  end
end
