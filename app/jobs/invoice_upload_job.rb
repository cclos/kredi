# frozen_string_literal: true

class InvoiceUploadJob < ApplicationJob
  queue_as :default

  def perform(files_content)
    invoices = []
    emitters = []
    receivers = []

    files_content.each do |xmlfile|
      hash = Hash.from_xml(xmlfile)
      data = clean(hash['hash'])
      invoices.append(data[:invoice])
      emitters.append(data[:emitter])
      receivers.append(data[:receiver])
    end

    ActiveRecord::Base.transaction do
      process_emitters(emitters)
      process_receivers(receivers)
    end

    process_invoices(invoices)
  end

  def process_invoices(data)
    data.map! do |h|
      h.tap { |kv| kv[:receiver_id] = User.find_by_username(kv[:receiver_id]).id }
      h.tap { |kv| kv[:emitter_id] = User.find_by_username(kv[:emitter_id]).id }
    end

    Invoice.upsert_all(data, unique_by: :uuid)
  end

  def process_emitters(data)
    User.upsert_all(data, unique_by: :rfc)
  end

  def process_receivers(data)
    User.upsert_all(data, unique_by: :rfc)
  end

  def clean(record)
    {
      invoice: {
        uuid: record['invoice_uuid'],
        status: record['status'],
        amount: record['amount']['cents'],
        currency: record['amount']['currency'],
        emitted_at: record['emitted_at'],
        expires_at: record['expires_at'],
        cfdi_digital_stamp: record['cfdi_digital_stamp'],
        receiver_id: record['receiver']['rfc'],
        emitter_id: record['emitter']['rfc'],
        created_at: DateTime.now,
        updated_at: DateTime.now
      },
      emitter: {
        name: record['emitter']['name'],
        rfc: record['emitter']['rfc'],
        username: record['emitter']['rfc'],
        created_at: DateTime.now,
        updated_at: DateTime.now
      },
      receiver: {
        name: record['receiver']['name'],
        rfc: record['receiver']['rfc'],
        username: record['receiver']['rfc'],
        created_at: DateTime.now,
        updated_at: DateTime.now
      }
    }
  end
end
