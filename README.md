# Consideraciones técnicas

* Uso de  Rails 6 & Ruby 3.1.0 de acuerdo a la info brindada en el doc del challenge
* Postgres como base de datos
* Sidekiq / redis como broker para la tarea asíncrona de bulk upload usando transacciones y poder agilizar los upserts
* Docker compose para conectar servicios en red
* Nginx como reverse proxy

# Arquitectura

## Modelos

1. _User_ representa a un emisor y un receptor de facturas

1. _Invoice_ representa una factura, los atributos fueron tomados del xml de ejemplo

1. _Invoice_ contiene una llave primaria uuid, mientras que preserva dos llaves foráneas a User por medio de emitter, receiver

1. Uso de índice es algunos campos para optimizar los queries a base de datos y criterio de update / insert

# Análisis de código

* Uso de rubocop, se excluye spec, vendor, db, config y algunas excepciones para lineas de código largas

# Ejecución

## Docker
```
$ docker network create dev
$ docker volume create --name kredimx-postgres
$ docker volume create --name kredimx-redis
$ docker-compose run app bundle exec rails db:create
$ docker­ compose run app rake db:migrate
$ docker-compose up
```

Se anexa ejemplo de .env para las necesidades de ambiente en local

# Pruebas, Rubocop y Postman

* Uso de rspec para probar la mayoria de los casos de uso

```
$ docker-compose run app bundle exec rspec .
$ docker-compose run app bundle exec rubocop
```

Se incluye una colección de postman con un test run para poder autoasignar el token

