# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module App
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
    # config.cache_store = :redis_store, ENV['CACHE_URL'], { namespace: 'app::cache' }
    config.autoload_paths += %W[#{config.root}/lib]
    # config.autoload_paths << Rails.root.join('app/models')
    config.active_job.queue_adapter = :sidekiq
    # config.eager_load = true
  end
end
