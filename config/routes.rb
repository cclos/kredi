# frozen_string_literal: true

Rails.application.routes.draw do
  resources :users, param: :_username
  post '/auth/login', to: 'authentication#login'

  resources :invoices
  post '/upload', to: 'invoices#upload'
  get '/qr', to: 'invoices#qr', format: :svg
end
